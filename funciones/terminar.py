# si la funcion no retorna nada por defeto retorna None
def mi_funcion(parametro):
    if parametro:
        return 100


resultado = mi_funcion('a')
if resultado:
    print('El resultado no es None!')

# el * agrupará todos los argumentos dentro de una tupla
def suma(param_required, *args):
    total = 0
    print(param_required)
    for valor in args:
        total += valor
    return total

# el uso de 2 * nos agrupa en un diccionario


def user_authentiqued(**kwargs):
    print(kwargs)


def combinacion(requerido, *args, **kwargs):
    print(requerido)
    print(args)
    print(kwargs)

# resultado = suma("This is a required param", 10, 20, 30, 40, 50, 60, 70)
# print(resultado)

# user_authentiqued(codi=True, facilito=False)


combinacion('Valor requerido', 10, 20, valor_uno=True, valor_dos=False)

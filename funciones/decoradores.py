# a, b, c
# a(b) => c

def decorador(funcion):
  def nueva_funcion():
    print('Podemos agregar código antes')
    funcion()
    print('Podemos agregar código después')

  return nueva_funcion

@decorador
def funcion_a_decorar():
  print('Este es una función a decorar')

funcion_a_decorar()
'''
closure: cuando una funcion genere
dinámicamente otra función
'''

def mostrar_mensaje(mensaje):
  mensaje = mensaje.title() #local
  """GAAA"""

  def mostrar():
    print(mensaje)

  return mostrar

nueva_funcion = mostrar_mensaje('ola k ase')
nueva_funcion()

print(mostrar_mensaje.__doc__)
animal = 'León'  # Es una variable global


def mostrar_animal():
    global animal
    animal = 'Ballena'  # variable local
    print(animal)

# imprime dos veces ballena
mostrar_animal()
print(animal)

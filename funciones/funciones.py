def saluda():
    print('Hola mundo')


def crear_mensaje(nombre):
    mensaje = 'Hola {}, bienvenido al curso'.format(nombre)
    return mensaje


def suma(val1, val2, val3):
    return val1 + val2 + val3


def obtener_curso():
    return 'Curso de Python', 'Básico', 3.6


curso, nivel, version = obtener_curso()
print(curso, nivel, version)

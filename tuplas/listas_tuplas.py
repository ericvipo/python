lista = ['Curso', 'Python', 'CodigoFacilito']
tupla = ('Introducción', 'Básico', 'Listas', 'Tuplas')

mensaje = 'Este es el curso de Python'

# convirtiendo tupla a lista
# tmb puedes convertir de string a lista o tupla
nueva_lista = list(tupla)
nueva_tupla = tuple(lista)

print(nueva_lista)
print(nueva_tupla)
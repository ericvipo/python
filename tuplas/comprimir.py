tupla = (1,2,3,4,5,6, 7, 8, 9, 10)
# uno, dos, tres, cuatro = tupla[0], tupla[1], tupla[2], tupla[3]
# 6 elementos no pueden ser asignados a cuatro variables
# uno, dos, tres, cuatro = tupla
# uno, dos, tres, *resto_valores = tupla #lo del asterisco seria una lista
# uno, dos, tres, cuatro, *_ = tupla # solo chapa los 4 primeros
# uno, dos, tres, cuatro, *_, nueve, diez = tupla # omite los *_

uno, _, tres, cuatro, *_, nueve, diez = tupla

print(uno)
# print(dos)
print(tres)
print(cuatro)
# print(resto_valores)
print(nueve)
print(diez)
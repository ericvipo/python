# las tuplas son inmutables
# las tuplas son mucho más rápidas que
# las lista en cuanto a obtener elementos se refiere
tupla = (1,2,3,4,5,6,7,8,9,0)

elemento = tupla[4]

print(elemento)
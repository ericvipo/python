calificacion = 10  # input
color = None

if calificacion >= 7:
    color = 'verde'
else:
    color = 'rojo'

color = 'verde' if calificacion >= 7 else 'rojo'

print(calificacion, color)

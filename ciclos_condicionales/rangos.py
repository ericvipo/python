# for valor in range(1, 100, 2):
#     print(valor)

lista = [1, 2, 3, 4, 5, 6]
# for indice in range(len(lista)):
#     print('Indice: ', indice, 'Valor: ', lista[indice])
# con enumerate nosotros podemos recorrer un objeto iterable
# algo no tan comun es indicar el punto de partida 10 para el indice
for indice, valor in enumerate(lista, 10):
    print('Indice: ', indice, 'Valor: ', valor)

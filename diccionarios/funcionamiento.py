# no pueden existir llaves duplicadas
diccionario = {} # diccionario = dict()
diccionario['edad'] = 28
diccionario['nombrex'] = 'Codi'  # para agregar llave con su valor

res = 'z' in diccionario  # para saber si la llave existe
# puede obtener cualquier cosa como retorno
res = diccionario.get('nombre', 'La llave no existe')
res = diccionario.setdefault('nombre', {})

print(res)
print(diccionario)

diccionario = {'a': 1, 'b': 2, 'c': 3}

# resultado = diccionario.keys() # lista de keys
# resultado = tuple(resultado)
res = diccionario.values()
res = diccionario.items() # dict_items([('a', 1), ('b', 2), ('c', 3)])

print(res)

for key, value in diccionario.items():
  print(key, value)
diccionario = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5}

del diccionario['a']  # elimina llave y valor
valor = diccionario.pop('b')  # retorna el valor eliminado
diccionario = {}  # eliminando, diccionario vacio o dicccionario.clear()

print(diccionario)

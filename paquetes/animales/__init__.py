'''
Este archivo es necesario colocarlos dentro de un
folder para que Python sepa que es un paquete, puede
estar en blanco o no
'''
# Exposición de clases, funciones u objetos
from .aves import Pinguino #para no hacer from animales.aves import Pinguino
from .felinos import Jaguar

print('Este es un mensaje del archivo init')

def mi_funcion():
  print('My function')

mi_jaguar = Jaguar()
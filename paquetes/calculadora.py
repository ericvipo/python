
__author__ = "Eric Vilca Poma"
__copyright__ = "Cppyright 2021, GAAA"



def suma(val1, val2):
  return val1 + val2

def resta(val1, val2):
  return val1 - val2

def multiplicacion(val1, val2):
  return val1 * val2

def division(val1, val2):
  return val1 / val2


# atributo __name__

if __name__ == '__main__':
  print('Soy un mensaje de calculadora')
else:
  print('Estoy siendo usado como módulo')
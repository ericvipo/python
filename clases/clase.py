# pass: sentencia nula
"""
Todos los métodos de una clase deben
de recibir un parámetro
self: no es una palabra reservada, es convencion
"""
class Usuario:
  def __init__(
    self,
    username='',
    correo='',
    nombre=''
  ):
    self.username = username
    self.correo = correo
    self.nombre = nombre

  def saluda(self):
    return 'Hola, soy un usuario ' + self.nombre

codi = Usuario('ericvipo', 'eric@gmail.com', 'Eric')
eric = Usuario()

print(codi.saluda())
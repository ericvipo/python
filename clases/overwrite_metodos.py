class Animal:
  def comer(self):
    print('Comiendo')
  def dormir(self):
    print('Durmiendo')

class Mascota:
  def fecha_adopcion(self, fecha):
    self.fecha_de_adopcion = fecha

# Animal y Mascota deben estar arriba de Perro
class Perro(Animal, Mascota):
  def __init__(self, nombre):
    self.nombre = nombre
  def ladrar(self):
    print('Ladrando')
  def dormir(self):
    print(self.nombre, 'está durmiendo')
    Animal.dormir(self)
    print('No molestar')


pepe = Perro('Pepe')
pepe.dormir()
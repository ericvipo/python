class Animal:
  def comer(self):
    print('Comiendo')
  def dormir(self):
    print('Durmiendo')
  def comun(self):
    print('Este es un método de Animal')

class Mascota:
  def fecha_adopcion(self, fecha):
    self.fecha_de_adopcion = fecha
  def comun(self):
    print('Este es un método de Mascota')

# Animal y Mascota deben estar arriba de Perro
class Perro(Animal, Mascota):
  def __init__(self, nombre):
    self.nombre = nombre
  def ladrar(self):
    print('Ladrando')
  def comun(self):
    print('Este es un método de Perro')


pepe = Perro('Pepe')
# pepe.ladrar()
# pepe.comer()
# pepe.dormir()

pepe.fecha_adopcion('Hoy')

print(pepe.fecha_de_adopcion)

pepe.comun()

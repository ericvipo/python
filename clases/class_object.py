class Gato:
  def __init__(self, nombre):
    self.nombre = nombre
  def __str__(self):
    return self.nombre

class Pato(object):
  def __init__(self, nombre):
    self.nombre = nombre
  def __str__(self):
    return self.nombre

gato = Gato('Bigotes')
pato = Pato('Lucas')

print(gato)
print(pato)

print(gato.__dict__)
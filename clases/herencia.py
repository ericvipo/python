class Animal:
  def comer(self):
    print('Comiendo')
  
  def dormir(self):
    print('Durmiendo')


class Perro(Animal):
  def __init__(self, nombre):
    self.nombre = nombre

  def ladrar(self):
    print('Ladrando')

pepe = Perro('Pepe')
pepe.ladrar()
pepe.comer()
pepe.dormir()

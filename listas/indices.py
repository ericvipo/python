cursos = ['python', 'django', 'flask', 'c', 'c++', 'c#', 'java', 'php']

curso = cursos[-1] #negativo posición a la inversa
sub = cursos[:3] #sublista con los primeros 3 elementos
# [5:] desde la posicion 5 hast el final
# [1:7:2] del indice 1 hasta el 7 con saltos de 2
# [::2] desde el inicio de 2 en 2
# [::-1] invirtiendo una lista

print(sub)
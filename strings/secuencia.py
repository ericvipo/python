# los strings al igual q las tuplas son inmutables
curso = 'Curso de python 3'

# resultado = len(curso)
# resultado = curso[0]
# tmb puedes generar substrings

curso = 'c' + curso[1:] + ' ' + str(1551)

print(curso)
curso = "Python"
version = "3"

# resultado = "Curso de %s %s" %(curso, version)
# resultado = "Curso de {} {}".format(curso, version)
resultado = "Curso de {a} {b}".format(b=curso, a=version)

print(resultado)
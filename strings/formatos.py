texto = "curso de Python 3, Python básico"

# resultado = texto.capitalize()
# resultado = texto.swapcase()
# resultado = texto.upper()
# resultado = texto.lower()
# print(resultado.isupper())
# print(resultado.islower())
# nuevo string con formato de titulo
# resultado = resultado.title()
# Python será remplazado un sola vez por Ruby
resultado = texto.replace('Python', 'Ruby', 1)

# texto.strip() sin espacios al inicio y al final de los strings

print(resultado)